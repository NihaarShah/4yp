loaded_model = pickle.load(open('/Users/nihaar/Documents/4yp/4yp/Experiment Results/06:01:2018/balanced_training_set_model.pkl', 'rb'))
	# Converting the scores for the test set into probabilities of the test set predictions
    y_test_probabilities = loaded_model.predict_proba(x_test)
    # Thresholding the probabilities so that 
    y_test_predictions_high_recall_p3 = y_test_probabilities[:,1] > 0.4     

    average_precision = average_precision_score(y_test, y_test_predictions_high_recall_p3)