import scipy.io as scio
import numpy as np


#Path to positive features matlab array
positive_path = '/Users/nihaar/Documents/4yp/data/new-detections/taxi-rank-2/real_features_matrix.mat'
negative_path = '/Users/nihaar/Documents/4yp/data/new-detections/taxi-rank-2/fake_features_matrix.mat'

# Positive image samples 
pos_mat = scio.loadmat(positive_path)
positive_patch = pos_mat['real_features_matrix']

# Negative images 
neg_mat = scio.loadmat(negative_path)
negative_patch = neg_mat['fake_features_matrix']

i,_ = negative_patch.shape
j,_ = positive_patch.shape

# Split train and test data as 80%-20% of real and fake sets
i_train_boundary = np.ceil(0.85*i)
i_train_boundary = i_train_boundary.astype(int)

j_train_boundary = np.ceil(0.85*j)
j_train_boundary = j_train_boundary.astype(int)

# Splitting training and test set in positive examples
x_train_pos=positive_patch[0:j_train_boundary,:] 
x_test_pos=positive_patch[j_train_boundary+1:j,:] 

# Splitting training and test set in negative examples
x_train_neg=negative_patch[0:i_train_boundary,:] # Again leaving out 200 images;
x_test_neg = negative_patch[i_train_boundary+1:]

# Extracting the rows (i.e. no. of features) of all data matrices.
p,_ = np.shape(x_train_pos)
q,_ = np.shape(x_test_pos)
r,_ = np.shape(x_train_neg)
s,_ = np.shape(x_test_neg) 

# Now y stuff

# training labels for these images
y_train_pos = np.ones(p)
y_train_neg = np.zeros(r)
y_train = np.concatenate((y_train_pos,y_train_neg))
y_train = np.transpose(y_train)

# testing labels for the images
y_test_pos = np.ones(q)
y_test_neg = np.zeros(s)
y_test = np.concatenate((y_test_pos,y_test_neg))
y_test = np.transpose(y_test)


# Concatenating the positive and negative training and testing sets
x_train=np.concatenate((x_train_pos,x_train_neg),axis=0)
x_test=np.concatenate((x_test_pos,x_test_neg),axis=0)



# Randomly shuffling the rows of the feature matrix and corresponding target labels
y_train= np.reshape(y_train,[p+r,1])   
xy_combined = np.concatenate((x_train, y_train), axis=1)
np.random.shuffle(xy_combined) 
m,n = xy_combined.shape
y_train = xy_combined[:,n-1]
x_train = xy_combined[:,0:n-1]


# Same for test data
y_test = np.reshape(y_test,[q+s,1])
xy_combined = np.concatenate((x_test, y_test), axis=1)
np.random.shuffle(xy_combined) 
m,n = xy_combined.shape
y_test = xy_combined[:,n-1]
x_test = xy_combined[:,0:n-1]

# Starting the SVM stuff

import matplotlib.pyplot as plt
from sklearn import datasets, svm, metrics
from sklearn.svm import LinearSVC



# Create a classifier: a support vector classifier
classifier = svm.SVC(kernel='linear')

# We learn the digits on the first half of the digits
classifier.fit(x_train, y_train)

y_score = classifier.predict(x_test)

# Now predict the value of the digit on the second half:
expected = y_test
predicted = y_score


# Print average pr value
from sklearn.metrics import average_precision_score
average_precision = average_precision_score(y_test, y_score)

print('Average precision-recall score: {0:0.2f}'.format(
      average_precision))



print("Classification report for classifier %s:\n%s\n"
      % (classifier, metrics.classification_report(expected, predicted)))
print("Confusion matrix:\n%s" % metrics.confusion_matrix(expected, predicted))
