# Training which involves hard negative mining on the validation set
import scipy.io as scio
import numpy as np
from sklearn import datasets, svm, metrics
from sklearn.svm import LinearSVC
import pickle
from sklearn.model_selection import train_test_split
import sys
sys.path.insert(0, '/Users/nihaar/Documents/4yp/4yp/code/helpers/')
import load_test_data 
from sklearn.model_selection import train_test_split
from sklearn.metrics import average_precision_score

# #This script applies linear SVM on contrast stretched training images.
# #Path to positive features matlab array
# #Path to positive features matlab array
# positive_path1 = '/Users/nihaar/Documents/4yp/data/new-detections/taxi-rank-2/real_features_matrix.mat'
# positive_path2 = '/Users/nihaar/Documents/4yp/data/new-detections/outside-uni-parks-1/real_features_matrix.mat'
# positive_path3 = '/Users/nihaar/Documents/4yp/data/new-detections/nuffleld college 1/real_features_matrix.mat'
# positive_path4 = '/Users/nihaar/Documents/4yp/data/new-detections/lamb and flag 1/real_features_matrix.mat'
# positive_path5 = '/Users/nihaar/Documents/4yp/data/new-detections/broad street/real_features_matrix.mat'

# negative_path1 = '/Users/nihaar/Documents/4yp/data/new-detections/taxi-rank-2/fake_features_matrix.mat'
# negative_path2 = '/Users/nihaar/Documents/4yp/data/new-detections/outside-uni-parks-1/fake_features_matrix.mat'
# negative_path3 = '/Users/nihaar/Documents/4yp/data/new-detections/nuffleld college 1/fake_features_matrix.mat'
# negative_path4 = '/Users/nihaar/Documents/4yp/data/new-detections/lamb and flag 1/fake_features_matrix.mat'
# negative_path5 = '/Users/nihaar/Documents/4yp/data/new-detections/broad street/fake_features_matrix.mat'

# # Positive image samples 
# pos_mat1 = scio.loadmat(positive_path1)
# pp1 = pos_mat1['real_features_matrix']

# pos_mat2 = scio.loadmat(positive_path2)
# pp2 = pos_mat2['real_features_matrix']

# pos_mat3 = scio.loadmat(positive_path3)
# pp3 = pos_mat3['real_features_matrix']

# pos_mat4 = scio.loadmat(positive_path4)
# pp4 = pos_mat4['real_features_matrix']

# pos_mat5 = scio.loadmat(positive_path5)
# pp5 = pos_mat5['real_features_matrix']

# # Negative images 
# neg_mat1 = scio.loadmat(negative_path1)
# np1 = neg_mat1['fake_features_matrix']

# neg_mat2 = scio.loadmat(negative_path2)
# np2 = neg_mat2['fake_features_matrix']

# neg_mat3 = scio.loadmat(negative_path3)
# np3 = neg_mat3['fake_features_matrix']

# neg_mat4 = scio.loadmat(negative_path4)
# np4 = neg_mat4['fake_features_matrix']

# neg_mat5 = scio.loadmat(negative_path5)
# np5 = neg_mat5['fake_features_matrix']

# # Training dataset first
# #Joining all real feature matrices from various datasets into one big matrix
# positive_patch_train = np.concatenate((pp1,pp2,pp3),axis = 0)
# negative_patch_train = np1


# # # Testing dataset next
# # positive_patch_test = np.concatenate((pp4),axis = 0)
# # negative_patch_test = np.concatenate((np4),axis =0)

# # Validation dataset
# positive_patch_val = pp5
# negative_patch_val = np.concatenate((np2,np3,np5),axis =0)

# # Testing dataset next
# positive_patch_test = pp4
# negative_patch_test = np4


# # Concatenating the positive and negative training and testing sets
# x_train=np.concatenate((positive_patch_train,negative_patch_train),axis=0)
# x_test=np.concatenate((positive_patch_test,negative_patch_test),axis=0)
# x_val=np.concatenate((positive_patch_val,negative_patch_val),axis=0)

# # Extracting the rows (i.e. no. of features) of all data matrices.
# p,_ = np.shape(positive_patch_train)
# q,_ = np.shape(positive_patch_test)
# r,_ = np.shape(negative_patch_train)
# s,_ = np.shape(negative_patch_test) 
# t,_ = np.shape(positive_patch_val)
# u,_ = np.shape(negative_patch_val)

# # Now y stuff

# # training labels for these images
# y_train_pos = np.ones(p)
# y_train_neg = np.zeros(r)
# y_train = np.concatenate((y_train_pos,y_train_neg))
# y_train = np.transpose(y_train)

# # testing labels for the images
# y_test_pos = np.ones(q)
# y_test_neg = np.zeros(s)
# y_test = np.concatenate((y_test_pos,y_test_neg))
# y_test = np.transpose(y_test)

# # validation labels
# y_val_pos = np.ones(t)
# y_val_neg = np.zeros(u)
# y_val = np.concatenate((y_val_pos,y_val_neg))
# y_val = np.transpose(y_val)


x_train,y_train = load_test_data.load_train_data_SVM()
x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=0.33, random_state=42)
x_test,y_test = load_test_data.load_test_data_SVM()

# Starting the SVM stuff
import matplotlib.pyplot as plt
from sklearn import datasets, svm, metrics
from sklearn.svm import LinearSVC

# Create a classifier: a support vector classifier
classifier = svm.SVC(C=.05,kernel='linear',probability=True)
# Creating a file to which we will print the results
f = open('svm_hard_neg_min_results_31jan.txt','w')
f.write('SVM Hard negative mining report:')
f.close()
classifier.fit(x_train, y_train)
for rounds in range(0,4):
  print("Round started # %s"%(rounds))
  # Using the validation data to evaluate and later do HNM
  y_score = classifier.predict(x_val)
  y_proba = classifier.predict_proba(x_val)
  i = len(y_val)
  fp = []
  # Vector of indices of false-positives in the validation set
  for j in range(0,i):
    if y_val[j] == 0 and y_score[j] == 1:
      if y_proba[j,1] > 0.7:
          fp.append(j)

  # Create a matrix of false negative features and correasponding labels
  hn_mat = np.empty([0,1681])
  hn_mat = x_val[fp,:]
  y_hn = np.zeros(len(fp))

  # Add the fp and fn to the training set
  x_train = np.concatenate((x_train,hn_mat),axis=0)
  y_train = np.concatenate((y_train,y_hn))

  # Retrain
  classifier.fit(x_train, y_train)  

  # Test on the test set
  predicted = classifier.predict(x_test)

  # Calculate metrics
  expected = y_test
  average_precision = average_precision_score(expected, predicted)

  # Write results for each round to a file
  f = open('svm_hard_neg_min_results_31jan.txt','a')
  f.write('Round # %s\n'%(rounds))
  f.write('Number of false positives: %s\n'%(len(fp)))
  f.write('\n'+'Average precision-recall score: {0:0.2f}'.format(
        average_precision))
  f.write('\n'+ "Classification report for classifier %s:\n%s\n"% (classifier, metrics.classification_report(expected, predicted)))
  f.write('\n'+ "Confusion matrix:\n%s" % metrics.confusion_matrix(expected, predicted))
  f.close()

# # Visualising False negatives
# import matplotlib.pyplot as plt

# n = 10
# plt.figure(figsize=(20, 4))
# for i in range(1,n):
#     # display original
#     ax = plt.subplot(2, n, i)
#     plt.imshow(fp_mat[i,:].reshape(41, 41))
#     plt.gray()
#     ax.get_xaxis().set_visible(False)
#     ax.get_yaxis().set_visible(False)

# plt.show()
# Saving the final model to plot pr curve later
# Save the model weights
filename = 'svm_hard_neg_mined_cross_val_model_aligned.pkl'
pickle.dump(classifier, open(filename, 'wb'))

# Saving false positives as a matlab matrix for visualisation 
scio.savemat('false_negatives_svm.mat', mdict={'hn_mat': hn_mat})
