import pickle
import scipy.io as scio
import numpy as np
import sys
sys.path.insert(0, '/Users/nihaar/Documents/4yp/4yp/code/helpers/')
from load_test_data import load_test_data_SVM

loaded_model = pickle.load(open('/Users/nihaar/Documents/4yp/4yp/Experiment Results/06:01:2018/balanced_training_set_model.pkl', 'rb'))


# positive_path1 = '/Users/nihaar/Documents/4yp/data/new-detections/taxi-rank-2/real_features_matrix.mat'
# positive_path2 = '/Users/nihaar/Documents/4yp/data/new-detections/outside-uni-parks-1/real_features_matrix.mat'
# positive_path3 = '/Users/nihaar/Documents/4yp/data/new-detections/nuffleld college 1/real_features_matrix.mat'
# positive_path4 = '/Users/nihaar/Documents/4yp/data/new-detections/lamb and flag 1/real_features_matrix.mat'
# positive_path5 = '/Users/nihaar/Documents/4yp/data/new-detections/broad street/real_features_matrix.mat'

# negative_path1 = '/Users/nihaar/Documents/4yp/data/new-detections/taxi-rank-2/fake_features_matrix.mat'
# negative_path2 = '/Users/nihaar/Documents/4yp/data/new-detections/outside-uni-parks-1/fake_features_matrix.mat'
# negative_path3 = '/Users/nihaar/Documents/4yp/data/new-detections/nuffleld college 1/fake_features_matrix.mat'
# negative_path4 = '/Users/nihaar/Documents/4yp/data/new-detections/lamb and flag 1/fake_features_matrix.mat'
# negative_path5 = '/Users/nihaar/Documents/4yp/data/new-detections/broad street/fake_features_matrix.mat'


# pos_mat4 = scio.loadmat(positive_path4)
# pp4 = pos_mat4['real_features_matrix']

# neg_mat4 = scio.loadmat(negative_path4)
# np4 = neg_mat4['fake_features_matrix']

# # Testing dataset next
# positive_patch_test = pp4
# negative_patch_test = np4

# x_test=np.concatenate((positive_patch_test,negative_patch_test),axis=0)
# q,_ = np.shape(positive_patch_test)
# s,_ = np.shape(negative_patch_test)

# # testing labels for the images
# y_test_pos = np.ones(q)
# y_test_neg = np.zeros(s)
# y_test = np.concatenate((y_test_pos,y_test_neg))
# y_test = np.transpose(y_test)

x_test,y_test = load_test_data_SVM()


    y_test_probabilities = loaded_model.predict_proba(x_test)

    y_test_predictions_high_recall_p3 = y_test_probabilities[:,1] > 0.4     

    average_precision = average_precision_score(y_test, y_test_predictions_high_recall_p3)