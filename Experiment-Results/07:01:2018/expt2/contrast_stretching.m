% This script reads in a matrix of features, reshapes it into n
% image tensor. Then finds its dynamic range. Then does stretching.

% load('real_features_matrix.mat');
[m,n] = size(real_features_matrix);
w = sqrt(n); %assuming square patches
patches_tensor = reshape(real_features_matrix,[m,w,w]);

CS = zeros(m,w,w);
% Just verify that random image looks alright
for i = 1:m
    img(:,:) = patches_tensor(i,:,:);
    
    % find the dynamic range
    DR = stretchlim(img);
    
    % Contrast stretching
    img_stretched = imadjust(img, DR);
    
    % Save each contrast streteched (CS) image in a new tensor
    CS(i,:,:) = img_stretched;
end
save('contrast_stretched_tensor','CS');
 