import scipy.io as scio
import numpy as np
from sklearn import datasets, svm, metrics
from sklearn.svm import LinearSVC
import pickle

#This script applies linear SVM on contrast stretched training images.
#Path to positive features matlab array
#Path to positive features matlab array
positive_path1 = '/Users/nihaar/Documents/4yp/data/new-detections/taxi-rank-2/real_features_matrix.mat'
positive_path2 = '/Users/nihaar/Documents/4yp/data/new-detections/outside-uni-parks-1/real_features_matrix.mat'
positive_path3 = '/Users/nihaar/Documents/4yp/data/new-detections/nuffleld college 1/real_features_matrix.mat'
positive_path4 = '/Users/nihaar/Documents/4yp/data/new-detections/lamb and flag 1/real_features_matrix.mat'
positive_path5 = '/Users/nihaar/Documents/4yp/data/new-detections/broad street/real_features_matrix.mat'

negative_path1 = '/Users/nihaar/Documents/4yp/data/new-detections/taxi-rank-2/fake_features_matrix.mat'
negative_path2 = '/Users/nihaar/Documents/4yp/data/new-detections/outside-uni-parks-1/fake_features_matrix.mat'
negative_path3 = '/Users/nihaar/Documents/4yp/data/new-detections/nuffleld college 1/fake_features_matrix.mat'
negative_path4 = '/Users/nihaar/Documents/4yp/data/new-detections/lamb and flag 1/fake_features_matrix.mat'
negative_path5 = '/Users/nihaar/Documents/4yp/data/new-detections/broad street/fake_features_matrix.mat'

# Positive image samples 
pos_mat1 = scio.loadmat(positive_path1)
pp1 = pos_mat1['real_features_matrix']

pos_mat2 = scio.loadmat(positive_path2)
pp2 = pos_mat2['real_features_matrix']

pos_mat3 = scio.loadmat(positive_path3)
pp3 = pos_mat3['real_features_matrix']

pos_mat4 = scio.loadmat(positive_path4)
pp4 = pos_mat4['real_features_matrix']

pos_mat5 = scio.loadmat(positive_path5)
pp5 = pos_mat5['real_features_matrix']

# Negative images 
neg_mat1 = scio.loadmat(negative_path1)
np1 = neg_mat1['fake_features_matrix']

neg_mat2 = scio.loadmat(negative_path2)
np2 = neg_mat2['fake_features_matrix']

neg_mat3 = scio.loadmat(negative_path3)
np3 = neg_mat3['fake_features_matrix']

neg_mat4 = scio.loadmat(negative_path4)
np4 = neg_mat4['fake_features_matrix']

neg_mat5 = scio.loadmat(negative_path5)
np5 = neg_mat5['fake_features_matrix']

# Training dataset first
#Joining all real feature matrices from various datasets into one big matrix
positive_patch_train = np.concatenate((pp1,pp3,pp2,pp5),axis = 0)
negative_patch_train = np.concatenate((np1,np3),axis =0)


# # Testing dataset next
# positive_patch_test = np.concatenate((pp4),axis = 0)
# negative_patch_test = np.concatenate((np4),axis =0)

# Testing dataset next
positive_patch_test = pp4
negative_patch_test = np4


# Concatenating the positive and negative training and testing sets
x_train=np.concatenate((positive_patch_train,negative_patch_train),axis=0)
x_test=np.concatenate((positive_patch_test,negative_patch_test),axis=0)


# Extracting the rows (i.e. no. of features) of all data matrices.
p,_ = np.shape(positive_patch_train)
q,_ = np.shape(positive_patch_test)
r,_ = np.shape(negative_patch_train)
s,_ = np.shape(negative_patch_test) 

# Now y stuff

# training labels for these images
y_train_pos = np.ones(p)
y_train_neg = np.zeros(r)
y_train = np.concatenate((y_train_pos,y_train_neg))
y_train = np.transpose(y_train)

# testing labels for the images
y_test_pos = np.ones(q)
y_test_neg = np.zeros(s)
y_test = np.concatenate((y_test_pos,y_test_neg))
y_test = np.transpose(y_test)

# Now we have X matrix for training and testing and the corresponding y labels for each


# Randomly shuffling the rows of the feature matrix and corresponding target labels
y_train= np.reshape(y_train,[p+r,1])   
xy_combined = np.concatenate((x_train, y_train), axis=1)
np.random.shuffle(xy_combined) 
m,n = xy_combined.shape
y_train = xy_combined[:,n-1]
x_train = xy_combined[:,0:n-1]


# Same for test data
y_test = np.reshape(y_test,[q+s,1])
xy_combined = np.concatenate((x_test, y_test), axis=1)
np.random.shuffle(xy_combined) 
m,n = xy_combined.shape
y_test = xy_combined[:,n-1]
x_test = xy_combined[:,0:n-1]

# Starting the SVM stuff

# Create a classifier: a support vector classifier
classifier = svm.SVC(kernel='linear',probability=True)

# We learn the digits on the first half of the digits
classifier.fit(x_train, y_train)

# Save the model weights
filename = 'balanced_training_set_model.pkl'
pickle.dump(classifier, open(filename, 'wb'))

# y_score = classifier.predict(x_test)

# # Now predict the value of the digit on the second half:
# expected = y_test
# predicted = y_score


# # Print average pr value
# from sklearn.metrics import average_precision_score
# average_precision = average_precision_score(y_test, y_score)

# print('Average precision-recall score: {0:0.2f}'.format(average_precision))



print("Classification report for classifier %s:\n%s\n"(classifier, metrics.classification_report(expected, predicted)))
# print("Confusion matrix:\n%s" % metrics.confusion_matrix(expected, predicted))
