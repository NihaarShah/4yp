\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Motivation}{1}
\contentsline {paragraph}{}{1}
\contentsline {paragraph}{}{1}
\contentsline {paragraph}{}{1}
\contentsline {section}{\numberline {1.2}Our Approach}{1}
\contentsline {paragraph}{}{2}
\contentsline {section}{\numberline {1.3}Literature Review}{2}
\contentsline {paragraph}{}{2}
\contentsline {paragraph}{}{3}
\contentsline {paragraph}{}{3}
\contentsline {paragraph}{}{3}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{4}
\contentsline {subsubsection}{R-CNN}{5}
\contentsline {subsubsection}{YOLO}{5}
\contentsline {subsubsection}{Sliding-Window}{6}
\contentsline {section}{\numberline {1.4}Project Aims and Contributions}{6}
\contentsline {chapter}{\numberline {2}Data Acquisition and Background}{8}
\contentsline {section}{\numberline {2.1}Working of RADAR}{8}
\contentsline {paragraph}{}{8}
\contentsline {paragraph}{}{9}
\contentsline {paragraph}{}{10}
\contentsline {section}{\numberline {2.2}RADAR sensor}{11}
\contentsline {paragraph}{}{11}
\contentsline {section}{\numberline {2.3}LiDAR sensor}{11}
\contentsline {paragraph}{}{12}
\contentsline {paragraph}{}{12}
\contentsline {paragraph}{}{13}
\contentsline {paragraph}{}{13}
\contentsline {paragraph}{}{14}
\contentsline {section}{\numberline {2.4}Datasets}{14}
\contentsline {section}{\numberline {2.5}Evaluation Metrics}{15}
\contentsline {paragraph}{}{15}
\contentsline {paragraph}{}{16}
\contentsline {paragraph}{}{16}
\contentsline {paragraph}{}{16}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{17}
\contentsline {chapter}{\numberline {3}SVM Based RADAR Classification}{18}
\contentsline {section}{\numberline {3.1}Motivation}{18}
\contentsline {paragraph}{}{18}
\contentsline {section}{\numberline {3.2}Theory}{18}
\contentsline {subsubsection}{Hyper-parameter tuning via Cross Validation}{19}
\contentsline {paragraph}{}{19}
\contentsline {section}{\numberline {3.3}Experiments}{20}
\contentsline {subsection}{\numberline {3.3.1}Aligned car patches}{20}
\contentsline {paragraph}{}{20}
\contentsline {subsection}{\numberline {3.3.2}Randomly oriented car patches}{21}
\contentsline {subsection}{\numberline {3.3.3}Hard Negative Mining}{22}
\contentsline {paragraph}{}{22}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{24}
\contentsline {paragraph}{}{24}
\contentsline {paragraph}{}{24}
\contentsline {subsection}{\numberline {3.3.4}Denoising by Static Thresholding}{25}
\contentsline {paragraph}{}{25}
\contentsline {paragraph}{}{25}
\contentsline {section}{\numberline {3.4}Success and Failures cases}{25}
\contentsline {paragraph}{}{25}
\contentsline {paragraph}{}{26}
\contentsline {paragraph}{}{26}
\contentsline {paragraph}{}{26}
\contentsline {section}{\numberline {3.5}Conclusion}{27}
\contentsline {paragraph}{}{27}
\contentsline {paragraph}{}{27}
\contentsline {chapter}{\numberline {4}CNN Based RADAR Classification}{28}
\contentsline {section}{\numberline {4.1}Motivation}{28}
\contentsline {paragraph}{}{28}
\contentsline {section}{\numberline {4.2}Theory}{28}
\contentsline {subsection}{\numberline {4.2.1}Encoder}{29}
\contentsline {paragraph}{}{29}
\contentsline {subsection}{\numberline {4.2.2}Loss}{29}
\contentsline {subsection}{\numberline {4.2.3}Learning by Optimisation}{30}
\contentsline {subsection}{\numberline {4.2.4}Back Propagation}{30}
\contentsline {subsection}{\numberline {4.2.5}Representation learning}{31}
\contentsline {subsection}{\numberline {4.2.6}Autoencoder}{31}
\contentsline {subsubsection}{Decoder}{31}
\contentsline {subsubsection}{Loss}{32}
\contentsline {section}{\numberline {4.3}Classifier Architecture}{33}
\contentsline {section}{\numberline {4.4}Experiments}{34}
\contentsline {subsection}{\numberline {4.4.1}Aligned Patches}{34}
\contentsline {subsection}{\numberline {4.4.2}Randomly oriented patches}{36}
\contentsline {subsubsection}{Results}{36}
\contentsline {subsection}{\numberline {4.4.3}Unsupervised Pre-Training}{37}
\contentsline {paragraph}{}{37}
\contentsline {subsection}{\numberline {4.4.4}Supervised Fine-Tuning}{38}
\contentsline {paragraph}{}{38}
\contentsline {paragraph}{}{39}
\contentsline {subsection}{\numberline {4.4.5}Regression for predicting orientation of car}{40}
\contentsline {section}{\numberline {4.5}SVM classifier Vs. CNN classifier}{40}
\contentsline {paragraph}{}{40}
\contentsline {section}{\numberline {4.6}Conclusion}{41}
\contentsline {chapter}{\numberline {5}Object Detection in a RADAR scan}{42}
\contentsline {section}{\numberline {5.1}Motivation}{42}
\contentsline {subsection}{\numberline {5.1.1}Sliding window}{42}
\contentsline {paragraph}{}{42}
\contentsline {paragraph}{}{42}
\contentsline {subsection}{\numberline {5.1.2}Non-Maximum Suppression}{43}
\contentsline {subsection}{\numberline {5.1.3}Hand Labelled Test Set}{43}
\contentsline {paragraph}{}{43}
\contentsline {subsection}{\numberline {5.1.4}Visualising Detections Radar}{44}
\contentsline {paragraph}{}{45}
\contentsline {section}{\numberline {5.2}Evaluating Detection}{46}
\contentsline {paragraph}{}{46}
\contentsline {subsection}{\numberline {5.2.1}Analysis of detection visuals}{46}
\contentsline {subsection}{\numberline {5.2.2}Time performance}{47}
\contentsline {paragraph}{}{47}
\contentsline {section}{\numberline {5.3}Conclusion}{47}
\contentsline {paragraph}{}{47}
\contentsline {paragraph}{}{47}
\contentsline {paragraph}{}{48}
\contentsline {paragraph}{}{48}
\contentsline {paragraph}{}{48}
